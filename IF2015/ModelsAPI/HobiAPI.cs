﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IF2015.ModelsAPI
{

    public class HobiAPI
    {
        public int id { get; set; }
        public string hobby_name { get; set; }
        public int rating { get; set; }
    }
}
