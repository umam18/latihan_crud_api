﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IF2015.Models;
using IF2015.ModelsAPI;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;

namespace IF2015.Controllers
{
    public class HobiController : Controller
    {
        private readonly BazingaContext context;

        public HobiController(BazingaContext context)
        {
            this.context = context;
        }

        [HttpPost]
        public IActionResult GetHobiList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var query = (from hobi in context.hobi
                            select new
                            {
                                id = hobi.id,
                                nama_hobi = hobi.nama_hobi,
                            });
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    query = query.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    query = query.Where(m => m.nama_hobi.Contains(searchValue));
                }
                recordsTotal = query.Count();
                var data = query.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IActionResult Index()
        {
            ViewData["Datatables"] = "ya";

            return View();
        }


        //Method for create form
        public IActionResult Create()
        {
            var model = new Hobi();

            IEnumerable<HobiAPI> hobiList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("https://localhost:44309/api/RatingHobbyAPI/GetAllData").Result;
            hobiList = response.Content.ReadAsAsync<IEnumerable<HobiAPI>>().Result;
            ViewBag.Hobi = hobiList;

            return PartialView("CreateEdit", model);
        }

        //Get data by ID
        public IF2015.Models.Hobi GetByID(int ID)
        {
            return context.hobi.Where(e => e.id == ID).FirstOrDefault();
        }

        //Method for edit form
        public IActionResult Edit(int ID)
        {
            var inputPassing = new Hobi();
            var existData = GetByID(ID);

            if (existData != null)
            {
                inputPassing.id = existData.id;
                inputPassing.nama_hobi = existData.nama_hobi;
            }

            IEnumerable<HobiAPI> hobiList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("https://localhost:44309/api/RatingHobbyAPI/GetAllData").Result;
            hobiList = response.Content.ReadAsAsync<IEnumerable<HobiAPI>>().Result;
            ViewBag.Hobi = hobiList;

            return PartialView("CreateEdit", inputPassing);
        }

        [HttpPost]
        //Execute create / edit
        public IActionResult CreateEdit(Hobi inputValue)
        {
            string message;
            if (inputValue.id == 0)
            {
                var mhs = new IF2015.Models.Hobi();
                mhs.nama_hobi = inputValue.nama_hobi;
                context.Add(mhs);
                context.SaveChanges();
                message = "Saved";
            }
            else
            {
                var existData = GetByID(inputValue.id);

                if (existData != null)
                {
                    existData.nama_hobi = inputValue.nama_hobi;
                    context.Update(existData);
                    context.SaveChanges();
                    message = "Updated";
                }
                else
                {
                    message = "Data not found";
                }
            }
            return Json(message);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var result = "";

            if (id != null)
            {
                context.hobi.Remove(context.hobi.Find(id));
                context.SaveChanges();
                result = "Deleted";
            }
            else
            {
                result = "Data not found";
            }

            return Json(result);
        }

    }
}
