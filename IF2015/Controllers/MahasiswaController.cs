﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IF2015.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace IF2015.Controllers
{
    public class MahasiswaController : Controller
    {
        private readonly BazingaContext context;

        public MahasiswaController(BazingaContext context)
        {
            this.context = context;
        }

        [HttpPost]
        public IActionResult GetMahasiswaList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var query = (from mhs in context.mahasiswa
                            select new
                            {
                                id = mhs.id,
                                nama_mahasiswa = mhs.nama_mahasiswa,
                                tanggal_lahir = mhs.tanggal_lahir,
                                jenis_kelamin = mhs.jenis_kelamin,
                                id_hobi = (from hobi in context.hobi where hobi.id == mhs.id_hobi select hobi.nama_hobi).FirstOrDefault(),
                            });
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    query = query.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    query = query.Where(m => m.nama_mahasiswa.Contains(searchValue)
                                            || m.id_hobi.Contains(searchValue)
                                        );
                }
                recordsTotal = query.Count();
                var data = query.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IActionResult Index()
        {
            ViewData["Datatables"] = "ya";

            return View();
        }


        //Method for create form
        public IActionResult Create()
        {
            var model = new Mahasiswa();
            ViewBag.Hobi = context.hobi.ToList();
            return PartialView("CreateEdit", model);
        }

        //Get data by ID
        public IF2015.Models.Mahasiswa GetByID(int ID)
        {
            return context.mahasiswa.Where(e => e.id == ID).FirstOrDefault();
        }

        //Method for edit form
        public IActionResult Edit(int ID)
        {
            var inputPassing = new Mahasiswa();
            var existData = GetByID(ID);

            if (existData != null)
            {
                inputPassing.id = existData.id;
                inputPassing.nama_mahasiswa = existData.nama_mahasiswa;
                inputPassing.tanggal_lahir = existData.tanggal_lahir;
                inputPassing.jenis_kelamin = existData.jenis_kelamin;
                inputPassing.id_hobi = existData.id_hobi;
            }

            ViewBag.Hobi = context.hobi.ToList();
            return PartialView("CreateEdit", inputPassing);
        }

        [HttpPost]
        //Execute create / edit
        public IActionResult CreateEdit(Mahasiswa inputValue)
        {
            string message;
            if (inputValue.id == 0)
            {
                var mhs = new IF2015.Models.Mahasiswa();
                mhs.nama_mahasiswa = inputValue.nama_mahasiswa;
                mhs.tanggal_lahir = inputValue.tanggal_lahir;
                mhs.jenis_kelamin = inputValue.jenis_kelamin;
                mhs.id_hobi = inputValue.id_hobi;
                context.Add(mhs);
                context.SaveChanges();
                message = "Saved";
            }
            else
            {
                var existData = GetByID(inputValue.id);

                if (existData != null)
                {
                    existData.nama_mahasiswa = inputValue.nama_mahasiswa;
                    existData.tanggal_lahir = inputValue.tanggal_lahir;
                    existData.jenis_kelamin = inputValue.jenis_kelamin;
                    existData.id_hobi = inputValue.id_hobi;
                    context.Update(existData);
                    context.SaveChanges();
                    message = "Updated";
                }
                else
                {
                    message = "Data not found";
                }
            }
            return Json(message);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            var result = "";

            if (id != null)
            {
                context.mahasiswa.Remove(context.mahasiswa.Find(id));
                context.SaveChanges();
                result = "Deleted";
            }
            else
            {
                result = "Data not found";
            }

            return Json(result);
        }

    }
}
