﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IF2015.Models;
using IF2015.ModelsAPI;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace IF2015.Controllers
{
    public class RatingHobbyController : Controller
    {
        [HttpPost]
        public IActionResult GetRatingHobbyList()
        {
            try
            {
                ICollection<HobiAPI> hobiList;
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("https://localhost:44309/api/RatingHobbyAPI/GetAllData").Result;
                hobiList = response.Content.ReadAsAsync<ICollection<HobiAPI>>().Result;

                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var query = (from hobi in hobiList.AsQueryable()
                             select new
                             {
                                 id = hobi.id,
                                 hobby_name = hobi.hobby_name,
                                 rating = hobi.rating,
                             });
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    query = query.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    query = query.Where(m => m.hobby_name.Contains(searchValue)
                                       || m.rating == Int16.Parse(searchValue));
                }
                recordsTotal = query.Count();
                var data = query.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IActionResult Index()
        {
            ViewData["Datatables"] = "ya";

            return View();
        }


        //Method for create form
        public IActionResult Create()
        {
            var model = new HobiAPI();

            return PartialView("CreateEdit", model);
        }

        //Get data by ID
        public IF2015.ModelsAPI.HobiAPI GetByID(int ID)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("https://localhost:44309/api/RatingHobbyAPI/GetHobby/"+ID).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<HobiAPI>(content);
            return result;
        }

        //Method for edit form
        public IActionResult Edit(int ID)
        {
            var inputPassing = new HobiAPI();
            var existData = GetByID(ID);

            if (existData != null)
            {
                inputPassing.id = existData.id;
                inputPassing.hobby_name = existData.hobby_name;
                inputPassing.rating = existData.rating;
            }

            return PartialView("CreateEdit", inputPassing);
        }

        [HttpPost]
        //Execute create / edit
        public IActionResult CreateEdit(HobiAPI inputValue)
        {
            string message = "";
            var jsonString = JsonConvert.SerializeObject(inputValue);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");

            if (inputValue.id == 0)
            {              
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsync("https://localhost:44309/api/RatingHobbyAPI/CreateHobby", content).Result;
                if(response.IsSuccessStatusCode)
                {
                    var result = response.StatusCode.ToString();
                    if(result == "OK")
                    {
                        message = "Saved";
                    }
                }
                else
                {
                    message = "Bad Create";
                }                
            }
            else
            {
                var existData = GetByID(inputValue.id);

                if (existData != null)
                {
                    HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsync("https://localhost:44309/api/RatingHobbyAPI/UpdateHobby/"+inputValue.id, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.StatusCode.ToString();
                        if (result == "OK")
                        {
                            message = "Updated";
                        }
                    }
                    else
                    {
                        message = "Bad Update";
                    }                    
                }
                else
                {
                    message = "Data not found";
                }
            }
            return Json(message);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            string message = "";

            if (id != 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("https://localhost:44309/api/RatingHobbyAPI/DeleteHobby/"+id).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.StatusCode.ToString();
                    if (result == "OK")
                    {
                        message = "Saved";
                    }
                }
                else
                {
                    message = "Bad Delete";
                }
            }
            else
            {
                message = "Data not found";
            }

            return Json(message);
        }

    }
}
