﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IF2015.Models
{
    public class BazingaContext: DbContext
    {
        public BazingaContext(DbContextOptions<BazingaContext> options) : base(options)
        {

        }
        public DbSet<Mahasiswa> mahasiswa { get; set; }

        public DbSet<Hobi> hobi { get; set; }
    }
}
