﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IF2015.Models
{

    public class Mahasiswa
    {
        [Key]
        public int id { get; set; }

        public string nama_mahasiswa { get; set; }

        [DataType(DataType.Date)]
        public DateTime? tanggal_lahir { get; set; }

        public string jenis_kelamin { get; set; }

        public int id_hobi { get; set; }
    }
}
