﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IF2015.Models
{

    public class Hobi
    {
        [Key]
        public int id { get; set; }
        public string nama_hobi { get; set; }
    }
}
