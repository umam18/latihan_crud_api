﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class DbAPIContext: DbContext
    {
        public DbAPIContext(DbContextOptions<DbAPIContext> options) : base(options)
        {

        }
        public DbSet<RatingHobbyAPI> rating_hobby { get; set; }
    }
}
