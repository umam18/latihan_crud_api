﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using WebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingHobbyAPIController : ControllerBase
    {
        private readonly DbAPIContext context;

        public RatingHobbyAPIController(DbAPIContext context)
        {
            this.context = context;
        }

        /*api/RatingHobbyAPI/GetAllData*/
        [HttpGet]
        [Route("GetAllData")]
        public ActionResult<IEnumerable<RatingHobbyAPI>> GetAllData()
        {
            var hobby = context.rating_hobby.ToList();
            if (hobby.Count <= 0)
            {
                return NotFound("Data not found!");
            }
            return Ok(hobby);
        }

        /*api/RatingHobbyAPI/GetHobby/3*/
        [HttpGet]
        [Route("GetHobby/{id}")]
        public ActionResult<RatingHobbyAPI> GetHobby(int id)
        {
            var GetHobby = context.rating_hobby.FirstOrDefault(x => x.id == id);
            if (GetHobby == null)
            {
                return NotFound("Data Not Found!");
            }
            return Ok(GetHobby);
        }

        /*api/RatingHobbyAPI/CreateHobby*/
        [HttpPost]
        [Route("CreateHobby")]
        public ActionResult<RatingHobbyAPI> CreateHobby(RatingHobbyAPI dataHobby)
        {
            if (dataHobby == null)
            {
                return NotFound("Failed entry data!");
            }
            else
            {
                var modelHobby = new RatingHobbyAPI();
                modelHobby.hobby_name = dataHobby.hobby_name;
                modelHobby.rating = dataHobby.rating;
                context.Add(modelHobby);
            }
            context.SaveChanges();
            return Ok(dataHobby);
        }

        /*api/RatingHobbyAPI/UpdateHobby*/
        [HttpPut]
        [Route("UpdateHobby/{id}")]
        public ActionResult<RatingHobbyAPI> UpdateHobby(int id, RatingHobbyAPI dataHobby)
        {
            if (id == 0)
            {
                return NotFound("Data not found!");
            }
            else
            {
                var existData = context.rating_hobby.Where(m => m.id == id).FirstOrDefault();
                if (existData != null)
                {
                    existData.hobby_name = dataHobby.hobby_name;
                    existData.rating = dataHobby.rating;
                    context.Update(existData);                    
                }
            }
            context.SaveChanges();
            return Ok(dataHobby);
        }

        [HttpDelete]
        [Route("DeleteHobby/{id}")]
        public ActionResult<RatingHobbyAPI> DeleteHobby(int id)
        {
            if (id == 0)
            {
                return NotFound("Data not found!");
            }
            else
            {
                var searchID = context.rating_hobby.Where(m => m.id == id).FirstOrDefault();
                if (searchID != null)
                {
                    context.rating_hobby.Remove(searchID);
                }
                else
                {
                    return NotFound("Data not found!");
                }
            }
            context.SaveChanges();
            return Ok("Successfully deleted data!");
        }
    }
}
